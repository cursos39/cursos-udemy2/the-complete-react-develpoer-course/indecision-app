class IndecisionApp extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      options: []
    }
    this.handleDeleteOptions = this.handleDeleteOptions.bind(this)
    this.handleAddOption = this.handleAddOption.bind(this)
    this.handlePick = this.handlePick.bind(this)
    this.handleDeleteOption = this.handleDeleteOption.bind(this)
  }

  componentDidMount () {
    try {
      const optionsJson = localStorage.getItem('options')
      const options = JSON.parse(optionsJson)
      if (options) {
        this.setState(() => ({ options }))
      }
    } catch (e) {
      // Do nothing at all
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevState.options.length !== this.state.options.length) {
      const optionsJson = JSON.stringify(this.state.options)
      localStorage.setItem('options', optionsJson)
    }
  }

  handleDeleteOptions () {
    this.setState(() => ({ options: [] }))
  }

  handleDeleteOption (optionToRemove) {
    this.setState((prevState) => ({
      options: prevState.options.filter(option => option !== optionToRemove)
    })
    )
  }

  handleAddOption (userOption) {
    if (!userOption) {
      return 'You have to input valid value'
    }
    if (this.state.options.find(option => option === userOption)) {
      return 'This option already exsits'
    }
    this.setState(({ options: prevOptions }) => ({
      options: prevOptions.concat([userOption])
    }))
  }

  handlePick () {
    const randomIndexOption = Math.floor(Math.random() * this.state.options.length)
    const randomOption = this.state.options[randomIndexOption]
    alert(randomOption)
  }

  render () {
    const subtitle = 'Put your life in the hands of the computer'
    return (
      <div>
        <Header subtitle={subtitle}/>
        <Action
          hasOptions={this.state.options.length > 0}
          handlePick={this.handlePick}
        />
        <Options
         options={this.state.options}
         handleDeleteOptions={this.handleDeleteOptions}
         handleDeleteOption={this.handleDeleteOption}
        />
        <AddOption
          handleAddOption={this.handleAddOption}
        />
      </div>
    )
  }
}

const Header = ({ title, subtitle }) => (
  <div>
    <h1>{title}</h1>
    {subtitle && <h2>{subtitle}</h2>}
  </div>
)

Header.defaultProps = {
  title: 'Indecision'
}

const Action = ({ handlePick, hasOptions }) => {
  return (
    <div>
      <button onClick={handlePick} disabled={!hasOptions}>
        What should I do?
      </button>
    </div>
  )
}

const Options = ({ handleDeleteOptions, options, handleDeleteOption }) => (
  <div>
    <button onClick={handleDeleteOptions}>Remove all</button>
    {options.length === 0 && <p>Please add an option to get started!</p>}
    <ol>
      {options.map((option, optionIndex) => (
        <Option
          key={optionIndex}
          optionText={option}
          handleDeleteOption={handleDeleteOption}
        />
      ))}
    </ol>
  </div>
)

const Option = ({ optionText, handleDeleteOption }) => (
  <div>
    <li>
      {optionText}
      <button onClick={(event) => handleDeleteOption(optionText)}>Remove</button>
    </li>
  </div>
)

class AddOption extends React.Component {
  constructor (props) {
    super(props)
    this.handleAddOption = this.handleAddOption.bind(this)
    this.state = {
      error: undefined
    }
  }

  handleAddOption (event) {
    event.preventDefault()

    const userOption = event.target.elements.option.value.trim()
    const error = this.props.handleAddOption(userOption)
    this.setState(() => ({ error }))
    if (!error) {
      event.target.elements.option.value = ''
    }
  }

  render () {
    return (
      <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.handleAddOption}>
          <input type="text" name="option"/>
          <button>Add Option</button>
        </form>
      </div>
    )
  }
}

ReactDOM.render(<IndecisionApp />, document.getElementById('app'))
