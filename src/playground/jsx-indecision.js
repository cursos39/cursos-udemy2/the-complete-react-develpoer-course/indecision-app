const appRoot = document.getElementById('app')
const app = {
  title: 'Indecision App',
  subtitle: 'React Native Indecision App',
  options: []
}

const onFormSubmit = (event) => {
  event.preventDefault()

  const userOption = event.target.elements.option.value

  if (userOption) {
    app.options.push(userOption)
    event.target.elements.option.value = ''
    renderAppTemplate()
  }
}

const removeOptions = () => {
  app.options = []
  renderAppTemplate()
}

const onMakeDecision = () => {
  const randomIndexOption = Math.floor(Math.random() * app.options.length)
  const randomOption = app.options[randomIndexOption]
  console.log(randomOption)
}

const renderAppTemplate = () => {
  const appTemplate = (
    <div>
      <h1>{app.title}</h1>
      {app.subtitle && <p>{app.subtitle}</p>}
      <p>{(app.options && app.options.length > 0) ? `Here are your options: ${app.options.join(', ')}` : 'No options'}</p>
      <button disabled={app.options.length === 0} onClick={onMakeDecision}>What should I do?</button>
      <button onClick={removeOptions}>Remove All</button>
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option"/>
        <button>Add Option</button>
      </form>
      <ol>
        {app.options.map((option, optionIndex) => <li key={optionIndex}>{option}</li>)}
      </ol>
    </div>
  )

  ReactDOM.render(appTemplate, appRoot)
}

renderAppTemplate()
