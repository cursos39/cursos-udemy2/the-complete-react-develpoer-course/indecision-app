class VisibilityToggle extends React.Component {
  constructor (props) {
    super(props)
    this.handleToggleVisibility = this.handleToggleVisibility.bind(this)
    this.state = {
      visibility: false
    }
    this.details = 'Alberto Eyo details'
  }

  handleToggleVisibility () {
    this.setState(({ visibility: prevVisibility }) => ({ visibility: !prevVisibility }))
  }

  render () {
    return (
      <div>
        <h1>Visibity Toggle</h1>
        <button onClick={this.handleToggleVisibility}>
          {this.state.visibility ? 'Hide details' : 'Show details'}
        </button>
        {this.state.visibility && <p>{this.details}</p>}
      </div>
    )
  }
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'))
