class Person {
  constructor (name = 'Anonymous', age = 0) {
    this.name = name
    this.age = age
  }

  getGreeting () {
    return `Hi. I am ${this.name}!`
  }

  getDescription () {
    const yearString = this.age === 1 ? 'year' : 'years'
    return `${this.name} is ${this.age} ${yearString} old.`
  }
}

class Student extends Person {
  constructor (name, age, major) {
    super(name, age)
    this.major = major
  }

  hasMajor () {
    return !!this.major
  }

  getDescription () {
    if (!this.hasMajor()) {
      return super.getDescription()
    }
    return `${super.getDescription()} Their major is ${this.major}.`
  }
}

class Traveler extends Person {
  constructor (name, age, homeLocation) {
    super(name, age)
    this.homeLocation = homeLocation
  }

  getGreeting () {
    if (!this.homeLocation) {
      return super.getGreeting()
    }
    return `${super.getGreeting()} I'm visiting from ${this.homeLocation}.`
  }
}

const me = new Traveler('Alberto Eyo', 1, 'Pontevedra')
console.log(me.getGreeting())

const other = new Traveler()
console.log(other.getGreeting())
