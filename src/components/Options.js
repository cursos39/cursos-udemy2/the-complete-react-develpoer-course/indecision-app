import React from 'react'
import Option from './Option.js'

const Options = ({ handleDeleteOptions, options, handleDeleteOption }) => (
  <div>
    <div className="widget-header">
      <h3 className="widget-header__title">Your Options</h3>
      <button onClick={handleDeleteOptions} className="button button--link">Remove all</button>
    </div>
    {options.length === 0 && <p className="widget__message">Please add an option to get started!</p>}
    {options.map((option, optionIndex) => (
      <Option
        key={optionIndex}
        optionText={option}
        count={optionIndex + 1}
        handleDeleteOption={handleDeleteOption}
      />
    ))}
  </div>
)

export default Options
